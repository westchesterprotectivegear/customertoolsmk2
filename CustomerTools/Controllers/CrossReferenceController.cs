﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CustomerTools.Models;
using CustomerTools.Services;
using Ninject;
using ProductManager.Entities;

namespace CustomerTools.Controllers
{
    public class CrossReferenceController : ApiController
    {
        [Inject]
        public IRepository<CrossReference, ProductManagerDatabaseContext> xRefRepo { get; set; }
        [Inject]
        public ISAPService sapService { get; set; }
        [Inject]
        public IMapper Mapper { get; set; }

        public List<GetCrossReferenceModel> Get(string xrefNumber, Guid key)
        {
            var model = new List<GetCrossReferenceModel>();

            if (key == Guid.Parse("8b6dd773-c78a-4b29-a399-a7c4c20b26d8"))
            {
                var xrefs = xRefRepo.GetGroup(x => x.CrossReferenceNumber.Contains(xrefNumber)).ToList();
                model = Mapper.Map<List<GetCrossReferenceModel>>(xrefs);
            }

            return model;
        }
    }
}
