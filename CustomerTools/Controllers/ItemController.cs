﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CustomerTools.Models;
using CustomerTools.Services;
using Ninject;
using ProductManager.Entities;

namespace CustomerTools.Controllers
{
    public class ItemController : ApiController
    {
        [Inject]
        public IRepository<PortalItem, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<PriceListData, ProductManagerDatabaseContext> priceListRepo { get; set; }
        [Inject]
        public IRepository<CustomerToolsAccount, ProductManagerDatabaseContext> accountRepo { get; set; }
        [Inject]
        public ISAPService sapService { get; set; }
        [Inject]
        public IMapper Mapper { get; set; }

        // GET api/values
        public IEnumerable<string> Get(Guid key)
        {
            var account = accountRepo.Get(x => x.PrivateKey == key);

            if (account != null)
            {
                var priceList = itemRepo.GetGroup(x => x.CustomerNumber == account.CustomerNumber);
                return priceList.Select(x => x.SKU);
            }
            return null;
        }

        public GetItemModel Get(string sku, Guid key)
        {
            var model = new GetItemModel();
            var account = accountRepo.Get(x => x.PrivateKey == key);

            if (account != null)
            {
                try
                {
                    var item = itemRepo.Get(x => x.SKU == sku.ToUpper() && x.CustomerNumber == account.CustomerNumber);
                    model = Mapper.Map<GetItemModel>(item);
                    model.QuantityOnHand = sapService.GetItemAvailability(sku.ToUpper(), account.CustomerNumber);
                    model.Message = "Success";
                }
                catch (Exception ex)
                {
                    model.Message = "Error - "+ex.Message;
                }
            }
            else
            {
                model.Message = "Invalid Private Key";
            }

            return model;
        }
    }
}
