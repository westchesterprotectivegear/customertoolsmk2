﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using Ninject;
using ProductManager.Entities;
using SAP.Middleware.Connector;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomerTools.Services
{
    public interface ISAPService
    {
        int GetItemAvailability(string sku, string customerNumber);
    }

    public class SAPService : ISAPService
    {
        [Inject]
        public IRepository<PriceListData, ProductManagerDatabaseContext> priceListRepo { get; set; }

        public int GetItemAvailability(string sku, string customerNumber)
        {
            DataTable table = new DataTable();

            RfcDestination dest = RfcDestinationManager.GetDestination("PRD");
            RfcRepository repo = dest.Repository;
            IRfcFunction testfn = repo.CreateFunction("MD_STOCK_REQUIREMENTS_LIST_API");
            SqlConnection conn;
            SqlCommand cmd;
            string q = "";
            double temp;

            testfn.SetValue("MATNR", sku.ToUpper().PadSAP(18));
            testfn.SetValue("WERKS", "MONR");

            testfn.Invoke(dest);
            table = testfn.GetTable("MDEZX").ToDataTable("mrpdetail");

            int runningTotal = 0;
            DateTime today = DateTime.Now;
            DateTime nextWeek = today.AddDays(7);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                var row = table.Rows[i];

                if (row["DELB0"].ToString() == "Stock")
                {
                    runningTotal += (int)(Convert.ToInt32(row["MNG01"]));
                    q = "SELECT SUM(GESME) AS Unavailable FROM prd.LQUA WHERE(LGTYP > '003') AND(MANDT = '100') AND(MATNR = '" + sku + "')";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
                    conn.Open();
                    cmd = new SqlCommand(q, conn);
                    var results = cmd.ExecuteReader();
                    if (results.HasRows)
                    {
                        results.Read();
                        if (Double.TryParse(results["Unavailable"].ToString(),out temp))
                        {
                            runningTotal = (int)(runningTotal - temp);
                        }
                    }
                } else if ((row["DELB0"].ToString() == "OutDel" || row["DELB0"].ToString() == "CusOrd" || row["DELB0"].ToString() == "DepReq") && row["DAT00"].ToString().ParseSAPDate()>today)
                {
                    runningTotal -= Math.Abs((int)(Double.Parse(row["MNG01"].ToString())));
                }

                /*if (row["DELB0"].ToString() == "Stock")
                {
                    runningTotal += (int)(Convert.ToInt32(row["MNG01"]) * .7);
                    
                        break;
                }

                //if (row["DAT00"].ToString().ParseSAPDate() > today && row["DELB0"].ToString() != "Stock" && row["DELB0"].ToString() != "SafeSt")
                //{
                //    for (int j = i; j < table.Rows.Count; j++)
                //    {
                //        var row2 = table.Rows[j];

                //        if (row["DAT00"].ToString().ParseSAPDate() <= nextWeek && row2["DELB0"].ToString() == "CusOrd")
                //        {
                //            runningTotal += Convert.ToInt32(row2["MNG01"]);
                //        }
                //        else
                //            break;
                //    }

                //    break;
                //}

                //if (row["DELB0"].ToString() != "SafeSt")
                //    runningTotal += Convert.ToInt32(row["MNG01"]);

    */
            }

            // if running total is less than 0, set it to 0
            runningTotal = (runningTotal < 0) ? 0 : runningTotal;
            runningTotal = (int)(runningTotal * .7);

            return runningTotal;
        }

        private IRfcFunction GetFunction()
        {
            int retries = 3;
            while (true)
            {
                try
                {
                    RfcDestination dest = RfcDestinationManager.GetDestination("PRD");
                    RfcRepository repo = dest.Repository;
                    return repo.CreateFunction("MD_STOCK_REQUIREMENTS_LIST_API");
                }
                catch
                {
                    if (--retries == 0) throw;
                    else Thread.Sleep(1000);
                }
            }
        }
    }
}