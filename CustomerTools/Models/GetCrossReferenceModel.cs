﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerTools.Models
{
    public class GetCrossReferenceModel
    {
        public string BasePartNumber { get; set; }
        public string Company { get; set; }
        public string CrossReferenceNumber { get; set; }
    }
}