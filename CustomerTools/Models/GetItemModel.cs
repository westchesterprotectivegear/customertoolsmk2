﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerTools.Models
{
    public class GetItemModel
    {
        public string Message { get; set; }
        public string BasePartNumber { get; set; }
        public string Description { get; set; }
        public string MarketingCopy { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int QuantityOnHand { get; set; }
        public string Size { get; set; }
        public string SKU { get; set; }
        public string UnitOfMeasure { get; set; }
        public string UPC { get; set; }
    }
}