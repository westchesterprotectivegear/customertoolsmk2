﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;

namespace CustomerTools
{
    public interface DatabaseContext
    {
        ISessionFactory GetSessionFactory();
    }

    public class SAPDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {

            return Fluently.Configure()
                          .Database(MsSqlConfiguration.MsSql2000.ShowSql().ConnectionString(c => c.FromConnectionStringWithKey("SAPConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("SAP.Entities")))
                          .ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }

    public class ProductManagerDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromConnectionStringWithKey("ProductManagerConnection")))
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Entities")))
                    .CurrentSessionContext<WebSessionContext>()
                    .BuildSessionFactory();
        }
    }
}