﻿using System.Linq;
using AutoMapper;
using ProductManager.Entities;
using CustomerTools.Models;

public static class AutoMapperConfig
{
    public static IMapper LoadMapper()
    {
        var config = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<PortalItem, GetItemModel>().ReverseMap();
            cfg.CreateMap<CrossReference, GetCrossReferenceModel>().ReverseMap();
        });

        return config.CreateMapper();
    }
}