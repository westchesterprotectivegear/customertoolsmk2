﻿using System;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using SAP.Middleware.Connector;

namespace CustomerTools
{
    public class SAPDestinationConfig : IDestinationConfiguration
    {
        public bool ChangeEventsSupported()
        {
            return false;
        }

        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public RfcConfigParameters GetParameters(string destinationName)
        {

            RfcConfigParameters parms = new RfcConfigParameters();

            switch (destinationName)
            {
                case "PRD":
                    parms.Add(RfcConfigParameters.AppServerHost, "Jed");
                    parms.Add(RfcConfigParameters.SystemNumber, "00");
                    break;
                case "QAS":
                    parms.Add(RfcConfigParameters.AppServerHost, "Jethro");
                    parms.Add(RfcConfigParameters.SystemNumber, "01");
                    break;
                case "DEV":
                    parms.Add(RfcConfigParameters.AppServerHost, "Jethro");
                    parms.Add(RfcConfigParameters.SystemNumber, "00");
                    break;
            }

            parms.Add(RfcConfigParameters.Client, "100");
            parms.Add(RfcConfigParameters.User, "rfcuser");
            parms.Add(RfcConfigParameters.Password, "Con2Me");
            parms.Add(RfcConfigParameters.Language, "EN");
            parms.Add(RfcConfigParameters.PoolSize, "20");

            return parms;
        }
    }

    public static class IRfcTableExtentions
    {
        /// <summary>
        /// Converts SAP table to .NET DataTable table
        /// </summary>
        /// <param name="sapTable">The SAP table to convert.</param>
        /// <returns></returns>
        public static DataTable ToDataTable(this IRfcTable sapTable, string name)
        {
            DataTable adoTable = new DataTable(name);
            //... Create ADO.Net table.
            for (int liElement = 0; liElement < sapTable.ElementCount; liElement++)
            {
                RfcElementMetadata metadata = sapTable.GetElementMetadata(liElement);
                adoTable.Columns.Add(metadata.Name, GetDataType(metadata.DataType));
            }

            //Transfer rows from SAP Table ADO.Net table.
            foreach (IRfcStructure row in sapTable)
            {
                DataRow ldr = adoTable.NewRow();
                for (int liElement = 0; liElement < sapTable.ElementCount; liElement++)
                {
                    RfcElementMetadata metadata = sapTable.GetElementMetadata(liElement);

                    switch (metadata.DataType)
                    {
                        case RfcDataType.DATE:
                            ldr[metadata.Name] = row.GetString(metadata.Name).Substring(0, 4) + row.GetString(metadata.Name).Substring(5, 2) + row.GetString(metadata.Name).Substring(8, 2);
                            break;
                        case RfcDataType.BCD:
                            ldr[metadata.Name] = row.GetDecimal(metadata.Name);
                            break;
                        case RfcDataType.CHAR:
                            ldr[metadata.Name] = row.GetString(metadata.Name);
                            break;
                        case RfcDataType.STRING:
                            ldr[metadata.Name] = row.GetString(metadata.Name);
                            break;
                        case RfcDataType.INT2:
                            ldr[metadata.Name] = row.GetInt(metadata.Name);
                            break;
                        case RfcDataType.INT4:
                            ldr[metadata.Name] = row.GetInt(metadata.Name);
                            break;
                        case RfcDataType.FLOAT:
                            ldr[metadata.Name] = row.GetDouble(metadata.Name);
                            break;
                        default:
                            ldr[metadata.Name] = row.GetString(metadata.Name);
                            break;
                    }
                }
                adoTable.Rows.Add(ldr);
            }
            return adoTable;
        }

        private static Type GetDataType(RfcDataType rfcDataType)
        {
            switch (rfcDataType)
            {
                case RfcDataType.DATE:
                    return typeof(string);
                case RfcDataType.CHAR:
                    return typeof(string);
                case RfcDataType.STRING:
                    return typeof(string);
                case RfcDataType.BCD:
                    return typeof(decimal);
                case RfcDataType.INT2:
                    return typeof(int);
                case RfcDataType.INT4:
                    return typeof(int);
                case RfcDataType.FLOAT:
                    return typeof(double);
                default:
                    return typeof(string);
            }
        }
    }

    public static class SAPExtensions
    {
        public static string PadSAP(this String str, int length)
        {
            if (Regex.IsMatch(str, @"^\d+$"))
                return str.PadLeft(length, '0');
            else
                return str;
        }
        public static DateTime ParseSAPDate(this String sapDate)
        {
            return DateTime.ParseExact(sapDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        public static string ToSAPDate(this DateTime date)
        {
            return date.ToString("yyyyMMdd");
        }
    }
}
