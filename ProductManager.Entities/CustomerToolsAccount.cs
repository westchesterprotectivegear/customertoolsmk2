﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class CustomerToolsAccount
    {
        public virtual bool Active { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual int ID { get; set; }
        public virtual Guid PrivateKey { get; set; }
    }

    public sealed class CustomerToolsAccountMapping : ClassMap<CustomerToolsAccount>
    {
        public CustomerToolsAccountMapping()
        {
            Id(p => p.ID);
            Map(p => p.Active);
            Map(p => p.CustomerNumber);
            Map(p => p.PrivateKey);
        }
    }
}
