﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class PortalItem
    {
        public virtual string SKU { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual string MarketingCopy { get; set; }
        public virtual string Size { get; set; }
        public virtual string UPC { get; set; }
        public virtual double? PriceListPrice { get; set; }
        public virtual double? CustomerSpecificPrice { get; set; }
        public virtual double? Price
        {
            get
            {
                if (CustomerSpecificPrice != null)
                {
                    return CustomerSpecificPrice;
                }

                return PriceListPrice;
            }
        }
    }

    public sealed class PortalItemMapping : ClassMap<PortalItem>
    {
        public PortalItemMapping()
        {
            Id(p => p.SKU);
            Map(p => p.BasePartNumber);
            Map(p => p.CustomerNumber);
            Map(p => p.Name);
            Map(p => p.Description);
            Map(p => p.UnitOfMeasure);
            Map(p => p.MarketingCopy);
            Map(p => p.Size);
            Map(p => p.CustomerSpecificPrice);
            Map(p => p.PriceListPrice);
            Map(p => p.UPC);
        }
    }
}
